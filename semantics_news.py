from pymongo import MongoClient
import csv
import re
import json
import math

# Before testing please create database named DataAssignment3 and a collecion NewsData in DataAssignment3
# and do a INSERT document option to the newsData.json(Provided in MongoFiles folder) in the NewsData Collection.
try:
    client = MongoClient("mongodb://localhost/DataAssignment3")   # defaults to port 27017
                    
except:
    print("Could not connect to MongoDB")
        
db=client.DataAssignment3
collection=db.NewsData

print("Mongo Connection Success")
cnt =0;
myList=[];
myJson={};

fileCnt=1;
searchQuery = ('canada','university','dalhousie university','halifax','business');

for data in db.NewsData.find():
    writeNews = open("./op/news_articles/"+str(fileCnt)+".txt","w+",encoding="utf-8",newline='')
    contentStr = data['content']
    content = contentStr.encode('ascii','ignore').decode('ascii')
    content = content.replace('\n','')
    content = content.replace('\r','')
    content = re.sub(r'((\d+)(\s)(\w+)$)','',content)
    description= data['source']
    description= re.sub(r'[(\W*)(^\s)]','',description)
    content = re.sub(r'[(\W*)(^\s)]',' ',content)
    author = (str(data['author']))
    author= re.sub(r'[(\W*)(^\s)]',' ',author)
    title = data['title']
    title = re.sub(r'[(\W*)(^\s)]',' ',title)
    
    myJson={
        "Author" : str(author),
        "Title" :str(title),
        "Description" : str(description),
        "Content" :str(content)
     }
    db = str(myJson)
    db = db.replace("\'", "\"")
    writeNews.write(str(db))
    myList.append(myJson)
    fileCnt=fileCnt+1;                     
    cnt=cnt+1;

print("News Files Created")

print("Total records" +str(cnt))

fileCnt=1
cntOfCanada=0;
cntOfUniv=0;
cntOfDalU=0;
cntofHalifax=0;
cntOfBusiness=0;
cdf=0;
udf=0;
dudf=0;
hdf=0;
bdf=0;
while fileCnt<500:   
    
    with open("./op/news_articles/"+str(fileCnt)+".txt","r",encoding="utf-8") as f:
        
        news = json.load(f)
        newsContent = news['Content']

        if searchQuery[0] in newsContent:
           cntOfCanada=cntOfCanada+1;
        if searchQuery[1] in newsContent:
            cntOfUniv=cntOfUniv+1;
        if searchQuery[2] in newsContent:
            cntOfDalU=cntOfDalU+1;
        if searchQuery[3] in newsContent:
            cntofHalifax=cntofHalifax+1;
        if searchQuery[4] in newsContent:
            cntOfBusiness=cntOfBusiness+1;               
        fileCnt=fileCnt+1;

cdf= float(str(500/cntOfCanada));
udf= float(str(500/cntOfUniv));
dudf= float(str(500/cntOfDalU));
hdf=float(str(500/cntofHalifax));
bdf=float(str(500/cntOfBusiness));


with open('./op/TF-IDF.csv', mode='w',newline='') as csv_file:
    
    tfwriter = csv.writer(csv_file,delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    
    tfwriter.writerow(["Total Documents"] +[str(fileCnt)])
    tfwriter.writerow(['SearchQuery']+['df'] + ['N/df'] +['Log(N/df)'])
    tfwriter.writerow([str(searchQuery[0])]+[str(cntOfCanada)]+[cdf] +[(str(math.log10(cdf)))])
    tfwriter.writerow([str(searchQuery[1])]+[str(cntOfUniv)]+[udf] +[(str(math.log10(udf)))])
    tfwriter.writerow([str(searchQuery[2])]+[str(cntOfDalU)]+[dudf] +[(str(math.log10(dudf)))])
    tfwriter.writerow([str(searchQuery[3])]+[str(cntofHalifax)]+[hdf] +[(str(math.log10(hdf)))])
    tfwriter.writerow([str(searchQuery[4])]+[str(cntOfBusiness)]+[bdf] +[(str(math.log10(bdf)))])
    
print("TFIDF file created")

##---------------------------------------------------------------------------------------------

tfIdf=[];
myDict={};
fileCnt=1;
RelMaxList=list();
Freq=[];
CanadaCnt=[];
TotalWords =[];
FileNumber =[];
RelFreqDict ={};
FinalList=[];
Nof=0
while fileCnt<500:   
    
    with open("./op/news_articles/"+str(fileCnt)+".txt","r",encoding="utf-8") as f:
        
        news = json.load(f)
        newsContent = news['Content']
        t=newsContent.split()
        tfIdf.append(t)
        tot=len(t)
        CanadaCount=0;
        RelFreq=0;
        RelMax=0;
        c=0;
        if "canada" in t:
            CanadaCount=CanadaCount+1;
        myDict={
            "Article" : fileCnt,
            "Total Words":tot,
            "Frequency":CanadaCount
            }
        tfIdf.append(myDict)
        tot = float(tot)
        CanadaCount = float(CanadaCount)
        if CanadaCount!=0:
            c=c=1;
            TotalWords.append(tot)
            CanadaCnt.append(CanadaCount); # Frequency
            RelFreq=(CanadaCount/tot) # Relative Frequency
            FileNumber.append(fileCnt) # Article Number
            Freq.append(RelFreq)

            RelFreqDict ={
                'Documents in which Canada appeared':fileCnt,
                'Total Words (m)':tot,
                'Frequency (f)':CanadaCount

                }
            FinalList.append(RelFreqDict)
            RelMaxList.append(str(RelFreq) +","+ str(fileCnt))
            

        fileCnt=fileCnt+1;


with open('./op/RelativeFrequency.csv', mode='w',newline='') as csv_file:
    fieldnames = ['Documents in which Canada appeared','Total Words (m)','Frequency (f)']
    tfwriter = csv.DictWriter(csv_file, fieldnames=fieldnames)
    tfwriter.writeheader()
      
    for item in FinalList:
        tfwriter.writerow(item)

print("Highest relative frequency of the news article is " +str(max(RelMaxList)))

hfnar = open("./op/news_articles/141.txt","r",)
print(hfnar.read())
hfnar.close();

print("Done! All files Created")

