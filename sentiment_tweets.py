from pymongo import MongoClient
import csv

# Before testing please create database named DataAssignment3 and a collecion TwitterData in DataAssignment3
# and do a INSERT document option to the AlltweetData.json(Provided in MongoFiles folder) in the TwitterData Collection.
try:
    client = MongoClient("mongodb://localhost/DataAssignment3")   # defaults to port 27017
                    
except:
    print("Could not connect to MongoDB")
        
db=client.DataAssignment3
collection=db.TwitterData

fpos = open("./words/positive-words.txt","r+")
fngev = open("./words/negative-words.txt","r+")

myList=[];
bow={};
eachTweet =[];
allTweets=[];

posWordsList=[];
negWordsList=[];
allWordsList=[];
posWords= fpos.read()
negWords = fngev.read()

print("Mongo Connection Success")
cnt =0;

for data in db.TwitterData.find():
    tweet= data['text']
    w=tweet.split()
    myList.append(w)
    neutral=1;
    for each in w:
        eachTweet.append(str(each))
        if each in bow:
            bow[each]=bow[each]+1;
        else:
            bow[each] = 1
    neither=1;
    for compare in w:
        text="";
        if str(compare) in posWords:            
            text= {"Tweet":cnt,"Message/tweets":tweet,"Match":compare,"Polarity":"positive"}
            neither=0;
            posWordsList.append(compare)
            allWordsList.append(compare)
            
            break
        if str(compare) in negWords:           
            text={"Tweet":cnt,"Message/tweets":tweet,"Match":compare,"Polarity":"negative"}
            neither=0;
            negWordsList.append(compare)
            allWordsList.append(compare)
            break    
        if(neither==1):
            text={"Tweet":cnt,"Message/tweets":tweet,"Match":compare,"Polarity":"neutral"}
    allTweets.append(text)
    cnt=cnt+1;
    
with open('./op/sentimental_analysis.csv', mode='w',newline='') as csv_file:
    
    fieldnames = ['Tweet', 'Message/tweets', 'Match','Polarity']
    writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
    writer.writeheader()
    for wr in allTweets:
        writer.writerow(wr)

with open('./op/positiveWords.csv', mode='w',newline='') as csv_file:
    pwriter = csv.writer(csv_file)
    pwriter.writerow(["Positive Words"])
    for item in posWordsList:
        pwriter.writerow([item])

with open('./op/negativeWords.csv', mode='w',newline='') as csv_file:
    nwriter = csv.writer(csv_file)
    nwriter.writerow(["Negative Words"])
    for item in negWordsList:
        nwriter.writerow([item])

with open('./op/allWords.csv', mode='w',newline='') as csv_file:
    awriter = csv.writer(csv_file)
    awriter.writerow(["All Words"])
    
    for item in allWordsList:
        awriter.writerow([str(item)])

print("Success! All files Created")
fngev.close();
fpos.close();
